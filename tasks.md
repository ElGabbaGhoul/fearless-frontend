---
Here are the steps:

[x] 1. Create a new file in ghi/app/src named AttendeesList.js
[x] 2. Create a new AttendeesList function component in there and exporting it as the "default"
[x] 3. Move the table JSX and its contents from App.js to the new function
[x] 4. Import the AttendeesList from the AttendeesList.js module
[x] 5. Use the new AttendeesList function component in the App function component

---
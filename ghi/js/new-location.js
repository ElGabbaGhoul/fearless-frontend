// need to add an event listener for when the DOM loads
// declare a variable that will hold the url for the new api
// fetch tue url. don't forget to await so we get the response and not the promise
// if response is ok, lets get the data using the .json method. Don't forget to away that too.
window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/states/'
    const response = await fetch(url);

    if (!response.ok) {
        const data = await response.json();
        console.log(data);
    }
    }
);
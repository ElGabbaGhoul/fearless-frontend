window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();

        for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
        }
    // Here, add the 'd-none' class to the loading icon



    const loadingIcon = document.getElementById("loading-conference-spinner");
    loadingIcon.classList.add("d-none");
    selectTag.classList.remove('d-none');

    // Get the attendee form element by its id
    const formTag = document.getElementById("create-attendee-form");
    // add an event listener for the submit event
    formTag.addEventListener('submit', async event => {
        // prevent default from happening
        event.preventDefault();
        // Create a formData object from the form
        const formData = new formData(formTag);
        // Get a new object from the form data's entires
        const json = JSON.stringify(Object.fromEntries(formData));
        //const selectTag = document.getElementById('conference');
        const attendeeUrl = "http://localhost:8001/api/attendees/";
        // Create options for the fetch
        // Make the fetch using the await keyword to the URL (attendeeURL)
        const response = await fetch(attendeeUrl, {
            method:'POST',
            body: json,
            headers: {
                'Content-Type': 'application/json',

            },
        });
        // Check  to see the response
        console.log(response)
    })


        if (response.ok) {
            // reference the add and removal but instead to the successTag instead of select tag using d-none which hides it 
            const successTag = document.getElementById("success-message");
            formTag.classList.add('d-none');
            successTag.classList.remove('d-none');
        }
    }
});
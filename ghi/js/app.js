function createCard(pictureUrl, name, location, description, starts, ends) {
    return `
        <div class="container">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer bg-secondary text-light">
            <small class="">${starts}-${ends}</small>
            </div>
            <div id="liveAlertPlaceholder"></div>
            <button type="button" class="btn btn-primary" id="liveAlertBtn">Show live alert</button>
        </div>
    `;
}

var alertPlaceholder = document.getElementById('liveAlertPlaceholder')
var alertTrigger = document.getElementById('liveAlertBtn')

function alert(message, type) {
    var wrapper = document.createElement('div')
    wrapper.innerHTML = '<div class="alert alert-' + type + ' alert-dismissible" role="alert">' + message + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>'

    alertPlaceholder.append(wrapper)
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
    const response = await fetch(url);

    if (!response.ok) {
        alertTrigger.addEventListener('click', function () {
            alert('Nice, you triggered this alert message!', 'success')
        })
    } else {
        const data = await response.json();

        for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details.conference.description;
            const location = details.conference.location.name;
            const pictureUrl = details.conference.location.picture_url;
            const starts = new Date(details.conference.starts).toLocaleDateString();
            const ends = new Date(details.conference.ends).toLocaleDateString();
            const html = createCard(pictureUrl, name, location, description, starts, ends);
            const row = document.querySelector('.row');
            row.innerHTML += html;
        }
        }
    }
    } catch (e) {
      // Figure out what to do if an error is raised
    }

});

